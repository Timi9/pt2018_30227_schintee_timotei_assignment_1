/**
 * Write a description of class Monoame here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetPolinom;
public class Monoame
{
    private int coeff, pow;
    private int vizitat; //0 nevizitat si 1 vizitat

    public Monoame()    //constructor gol
    {
    }

    public Monoame(int coeff, int pow, int vizitat)  //constructor cu parametrii
    {
        this.coeff=coeff;
        this.pow=pow;
    }

    public int getPow()            //metoda pentru a obtine puterea unui monom
    {
        return this.pow;
    }

    public int getCoeff()         //metoda pentru a obtine coeficientul unui monom
    {
        return this.coeff;
    }

    public int getVizitat()      //metoda pentru a observa daca un monom a fost vizitat sau nu
    {
        return this.vizitat;
    }

    public void setPow(int pow)  //metoda pentru a modifica valoarea unei puteri
    {
        this.pow=pow;
    }

    public void setCoeff(int coeff)  //metoda pentru a modifica valoarea unui coeficient
    {
        this.coeff=coeff;
    }

    public void setVizitat(int vizitat) //metoda pentru a modifica valoarea pentru "vizitat"
    {
        this.vizitat=vizitat;
    }
}
