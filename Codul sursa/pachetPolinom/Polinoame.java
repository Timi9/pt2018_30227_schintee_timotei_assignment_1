
/**
 * Write a description of class Polinoame here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetPolinom;
import java.util.*;
import java.util.regex.*;
public class Polinoame
{
    private List<Monoame> lista_mon = new ArrayList<Monoame>(); //crearea listei "Monoame"

    public Polinoame()                            //crearea unui constructor gol
    {

    }
    
    public void adaugaMonom(Monoame m)                //adauga un monom in lista creata
    {
        lista_mon.add(m);
    }
    
    public List<Monoame> getMonoame()                //metoda care returneaza lista de monoame
    {
        return this.lista_mon;
    }

    public void citestePol(String s)                 //metoda pentru citirea unui polinom   
    {
       
        Pattern patt = Pattern.compile("(-?\\b\\d+)[xX]\\^(\\d+\\b)");
        Matcher m = patt.matcher(s);

        while (m.find()){
            Monoame monom= new Monoame(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)),0);
            lista_mon.add(monom);
        }
    }
    
    public String toString()       //metoda care returneaza monomul meu sub forma de string
    {
        String s="";
        for (Monoame m: lista_mon)
        {
            int coeficient= m.getCoeff();   //CREEZ O VARIABILA CARE PRIMESTE COEFICIENTUL MONOMULUI LA CARE MA AFLU
            if (coeficient>0)
                s+= "+"+ m.getCoeff() + "x^" + m.getPow();
            else
                s+=  m.getCoeff() + "x^" + m.getPow();
        }
        
        return s;
    }
    

}
