
/**
 * Write a description of class Operatii here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetOperatii;
import pachetPolinom.*;
import java.util.*;
public class Operatii
{
    public Polinoame adunare( Polinoame p1,Polinoame p2 ) //implementarea operatii de adunare
    {
        
        Polinoame rezultatPol = new Polinoame();

        for( Monoame m1 : p1.getMonoame() )
        {                                                                                              
            for( Monoame m2 : p2.getMonoame() )
            {
                if( m1.getPow() == m2.getPow() )                                                   
                {
                    m1.setVizitat(1);                                                                 
                    m2.setVizitat(1);

                    Monoame rezMonoame = new Monoame( m1.getCoeff() + m2.getCoeff(),m1.getPow( ),0);
                    rezultatPol.adaugaMonom(rezMonoame);
                }
            }
        }

        for( Monoame m1 : p1.getMonoame() )
        {
            if( m1.getVizitat() == 0 )                                                                 
                rezultatPol.adaugaMonom(m1);                                                                   
        }

        for( Monoame m2 : p2.getMonoame() ) 
        {
            if( m2.getVizitat() == 0 )
            {
                rezultatPol.adaugaMonom(m2);
            }
        }
        return rezultatPol;
    }

    public Polinoame scadere (Polinoame p1,Polinoame p2 )  //implementarea operatiei de scadere
    {
        Monoame rezMonoame;
        Polinoame rezultatPol = new Polinoame();

        for( Monoame m1 : p1.getMonoame() )
        {																							
            for( Monoame m2 : p2.getMonoame() )
            {
                if( m1.getPow() == m2.getPow() ) 													
                {
                    m1.setVizitat(1);																	
                    m2.setVizitat(1);
                    rezMonoame = new Monoame( m1.getCoeff() - m2.getCoeff(),m1.getPow( ),0);
                    rezultatPol.adaugaMonom(rezMonoame);
                }
            }
        }

        for( Monoame m1 : p1.getMonoame() )
        {
            if( m1.getVizitat() == 0 )																	
                rezultatPol.adaugaMonom(m1);																		
        }

        for( Monoame m2 : p2.getMonoame() ) 
        {
            if( m2.getVizitat() == 0 )
            {
                Monoame mon = new Monoame(-m2.getCoeff(),m2.getPow(),0);
                rezultatPol.adaugaMonom(mon);
            }
        }
        return rezultatPol;
    }

    public Polinoame inmultire(Polinoame p1, Polinoame p2) //implementarea operatiei de inmultire
    {
        
        Polinoame rezPol = new Polinoame();

        for (Monoame m1: p1.getMonoame())
            for( Monoame m2 : p2.getMonoame())
            {
                Monoame monom = new Monoame(m1.getCoeff() * m2.getCoeff(), m1.getPow()+m2.getPow(),0);
                rezPol.adaugaMonom(monom);
            }
        return rezPol;
    }

    public Polinoame derivare (Polinoame p)  //implementarea operatiei de derivare
    {
        
        Polinoame rezPol = new Polinoame();

        for (Monoame m: p.getMonoame())
        {
            Monoame rezMonoame = new Monoame( m.getCoeff() * m.getPow(), m.getPow()-1,0);
            rezPol.adaugaMonom(rezMonoame);
        }

        return rezPol;
    }

}
