
/**
 * Write a description of class AdunareButtonListener here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetInterfata;
import pachetPolinom.*;
import pachetOperatii.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class AdunareButtonListener implements ActionListener
{
    JTextField  txt1;     //Primul polinom dat ca text
    JTextField  txt2;     //Al doilea polinom dat ca text
    JTextField  txt3;     //Rezultatul operatiei dintre cele doua polinoame dat ca text

    public AdunareButtonListener(JTextField txt1, JTextField txt2, JTextField txt3){  //constructor cu parametrii
        this.txt1=txt1;
        this.txt2=txt2;
        this.txt3=txt3;
    }

    public void actionPerformed(ActionEvent e)   //metoda prin care, oricand se apasa pe butonul "adunare", se executa 
    {

        Polinoame a = new Polinoame();
        Polinoame b = new Polinoame();
        Polinoame c = new Polinoame();
       
        
        a.citestePol(txt1.getText());
        b.citestePol(txt2.getText());

        Operatii op= new Operatii();
        c=op.adunare(a,b);
        txt3.setText(c.toString());

    }
}
