
/**
 * Write a description of class AdunareButtonListener here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetInterfata;
import pachetPolinom.*;
import pachetOperatii.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class DerivareButtonListener implements ActionListener
{
    JTextField  txt1;
    JTextField  txt2;
    

    public DerivareButtonListener(JTextField txt1, JTextField txt2){
        this.txt1=txt1;
        this.txt2=txt2;
       
    }

    public void actionPerformed(ActionEvent e)  
    {

        Polinoame a = new Polinoame();
        Polinoame b = new Polinoame();

        a.citestePol(txt1.getText());
        
        Operatii op= new Operatii();
        b=op.derivare(a);
        
        txt2.setText(b.toString());

    }
}
