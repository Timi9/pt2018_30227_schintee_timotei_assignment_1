
/**
 * Write a description of class ResetareButtonListener here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetInterfata;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ResetareButtonListener implements ActionListener
{
    
    JTextField txt;

    public ResetareButtonListener(JTextField txt)
    {
        this.txt=txt;
    }

    public void actionPerformed(ActionEvent e)
    {
        txt.setText("");
    }
}