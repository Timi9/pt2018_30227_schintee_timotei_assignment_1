
/**
 * Write a description of class MyGUI here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetInterfata;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class MyGUI
{
    public static void main() {
        JFrame frame = new JFrame ("Calculator polinoame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);                       //Scrierea titlului in chenarul din stanga sus + setarea butonului x pentru exit
        frame.setSize(600, 500);

        JPanel panel1 = new JPanel(); 
        
        JTextField text_pol_a = new JTextField("");                                      
        text_pol_a.setPreferredSize( new Dimension(140,50));                       //Setarea dimensiunilor pentru primul spatiu, cel de dupa "Polinom 1"
        
        JButton b1 = new JButton("Resetare");
        b1.setPreferredSize(new Dimension(90,50));                                 //Setarea dimensiunilor pentru spatiul de dupa "Resetare"
        
        JLabel l1 = new JLabel ("Polinom a");                                      //Creare eticheta "Polinom a" + buton + spatiu pentru scris
        panel1.add(l1);
        panel1.add(text_pol_a);
        panel1.add(b1);
        panel1.setLayout(new FlowLayout());
        
        
        JPanel panel2 = new JPanel(); 
        
        JTextField text_pol_b = new JTextField("");
        text_pol_b.setPreferredSize( new Dimension(140,50));
        
        JButton b2 = new JButton("Resetare");
        b2.setPreferredSize( new Dimension(90,50));
        
        JLabel l2 = new JLabel ("Polinom b");
        panel2.add(l2);                                                             //Creare eticheta "Polinom b" + buton + spatiul pentru scris
        panel2.add(text_pol_b);
        panel2.add(b2);
        panel2.setLayout(new FlowLayout());


        JPanel panel3 = new JPanel();
        panel3.add(panel1);
        panel3.add(panel2);                                                //Crearea panelul 3, care contine panel 1 si panel 2
        panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));
        
        
        JPanel panel4 = new JPanel();
        JButton b3 = new JButton("adunare");
        JButton b4 = new JButton("scadere");                                  //Crearea panelului 4 care o sa contina 4 butoane, cele cu operatiile
        JButton b5 = new JButton("inmultire");
        JButton b6 = new JButton("derivare");
        
        panel4.add(b3);
        panel4.add(b4);
        panel4.add(b5);
        panel4.add(b6);
        panel4.setLayout(new FlowLayout());
        
        JPanel panel5 = new JPanel();
        JLabel l5 = new JLabel("Rezultat");
        JTextField text_afisare = new JTextField();                                    //Crearea panelului 5 care contine eticheta "Rezultat" + campul pentru afisare rezultat + dimensiuni
        text_afisare.setPreferredSize( new Dimension(140,50));
        panel5.add(l5);
        panel5.add(text_afisare);
        panel5.setLayout(new FlowLayout());
        
        JPanel panel6 = new JPanel();
        panel6.add(panel3);                                                           //Crearea unui panel 6 care sa cuprinda panel 3 + panel 4 + panel 5, toate asezate unul sub altul
        panel6.add(panel4);
        panel6.add(panel5);
        panel6.setLayout(new BoxLayout(panel6, BoxLayout.Y_AXIS));
        
        
        b1.addActionListener(new ResetareButtonListener(text_pol_a));
        b2.addActionListener(new ResetareButtonListener(text_pol_b));                 //creez ascultatori pentru toate butoanele
        
        b3.addActionListener(new AdunareButtonListener(text_pol_a,text_pol_b,text_afisare));
        b4.addActionListener(new ScadereButtonListener(text_pol_a,text_pol_b,text_afisare));
        b5.addActionListener(new InmultireButtonListener(text_pol_a,text_pol_b,text_afisare));
        b6.addActionListener(new DerivareButtonListener(text_pol_a,text_afisare));
        
        frame.setContentPane(panel6);
        frame.setVisible(true);      
        
    }
}
