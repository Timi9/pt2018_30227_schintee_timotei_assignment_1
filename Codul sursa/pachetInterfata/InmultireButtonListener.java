
/**
 * Write a description of class AdunareButtonListener here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
package pachetInterfata;
import pachetPolinom.*;
import pachetOperatii.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

public class InmultireButtonListener implements ActionListener
{
    JTextField  txt1;
    JTextField  txt2;
    JTextField  txt3;

    public InmultireButtonListener(JTextField txt1, JTextField txt2, JTextField txt3){
        this.txt1=txt1;
        this.txt2=txt2;
        this.txt3=txt3;
    }

    public void actionPerformed(ActionEvent e)  
    {

        Polinoame a = new Polinoame();
        Polinoame b = new Polinoame();
        Polinoame c = new Polinoame();

        a.citestePol(txt1.getText());
        b.citestePol(txt2.getText());

        Operatii op= new Operatii();
        c=op.inmultire(a,b);
        txt3.setText(c.toString());

    }
}
